# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.views.generic import View
from .models import Task
from .forms import TaskForm
from django import forms

class TaskList(View):
    def get(self, request):
        form = TaskForm()
        # form.fields['title'].widget = forms.HiddenInput()
        tasks = Task.objects.filter(user = request.user)
        return render(request, 'task_list.html', context={'form':form, 'tasks':tasks})

    def post(self, request):
        form = TaskForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            user = request.user
            task = Task(user = user, title = title)
            task.save()
            return redirect('todo-list')
        else:
            return redirect('todo-list')    

