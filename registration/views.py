# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
# from .forms import UserLogin
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect
from django.urls import reverse
from todoapp.models import Task
from django.contrib.auth.models import User
from django.contrib import messages
 
def admin_user(request):  
    data = {}
    for user in User.objects.all():
        data[user] = user.task_set.all()
    return render(request, 'admin_user.html', {'data':data})

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        # form.fields['field_name'].widget = forms.HiddenInput()
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('admin')
    else:
        form = UserCreationForm()
    return render(request, 'form.html', {'form':form})
    # return None



def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data = request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if user.is_superuser:
                return redirect('admin')
            else:
                return redirect('todo-list')
        else:
            messages.error(request,'username or password not correct')
            return redirect('login')  
    else:
        form = AuthenticationForm()
        return render(request, 'login.html', {'form': form})


def logout_view(request):
    if request.method == 'POST':
        logout(request)
    return redirect('login')



